FROM ubuntu:20.04
ENV TZ=America/Buenos_Aires
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
VOLUME /var/www/
RUN apt-get update
RUN apt-get install apache2 -y tzdata
RUN apt-get install -y php-gd libapache2-mod-php php-json php-mysql php-sqlite3 php-curl php-intl php-imagick php-zip php-xml php-mbstring php-soap php-ldap php-apcu php-redis php-dev libsmbclient php-gmp smbclient -y byobu curl git htop man unzip wget tar gzip bzip2 
RUN sed -i "s#html#owncloud#" /etc/apache2/sites-available/000-default.conf
RUN wget https://download.owncloud.org/community/owncloud-10.6.0.tar.bz2 && tar -jxvf owncloud-10.6.0.tar.bz2 -C /var/www/
ADD ./001-owncloud.conf /etc/apache2/sites-available/
RUN a2ensite 001-owncloud.conf
WORKDIR /var/www/owncloud
RUN a2enmod dir env headers mime rewrite setenvif
RUN chown -R www-data:www-data /var/www/owncloud
RUN a2enmod rewrite
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D",  "FOREGROUND"]
